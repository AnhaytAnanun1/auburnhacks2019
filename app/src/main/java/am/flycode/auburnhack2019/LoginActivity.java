package am.flycode.auburnhack2019;

import am.flycode.auburnhack2019.api.API;
import am.flycode.auburnhack2019.model.AppDataPreferences;
import am.flycode.auburnhack2019.model.UserResponse;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.text.InputType;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.container)
    View containerView;
    @BindView(R.id.phone)
    TextInputEditText phoneEditText;
    @BindView(R.id.code)
    TextInputEditText codeEditText;
    @BindView(R.id.change_login)
    AppCompatButton changeLoginButton;

    private AppDataPreferences appData;
    private boolean isCivilianLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        isCivilianLogin = true;

        appData = new AppDataPreferences(this);

        setupInput();
    }

    @Override
    public void onBackPressed() {
        if (isCivilianLogin) {
            Snackbar.make(containerView, R.string.enter_phone_first, Snackbar.LENGTH_SHORT).show();
        } else {
            Snackbar.make(containerView, R.string.enter_code_first, Snackbar.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.change_login)
    public void onChangeLogin(View view) {
        isCivilianLogin = !isCivilianLogin;

        setupInput();
    }

    @OnClick(R.id.proceed)
    public void onProceed(View view) {
        if (isCivilianLogin) {
            loginCivilian();
        } else {
            loginRescueTeam();
        }
    }

    private void setupInput() {
        phoneEditText.setText("");
        codeEditText.setText("");

        if (isCivilianLogin) {
            ((View)phoneEditText.getParent()).setVisibility(View.VISIBLE);
            ((View)codeEditText.getParent()).setVisibility(View.GONE);
            changeLoginButton.setText(R.string.rescue_team);
        } else {
            ((View)phoneEditText.getParent()).setVisibility(View.GONE);
            ((View)codeEditText.getParent()).setVisibility(View.VISIBLE);
            changeLoginButton.setText(R.string.civilian);
        }
    }

    private void loginRescueTeam() {
        if (codeEditText.getText() == null
                || codeEditText.getText().toString().isEmpty()) {

            Snackbar.make(containerView, R.string.enter_valid_code, Snackbar.LENGTH_SHORT).show();

            return;
        }

        String teamCode = codeEditText.getText().toString();

        if (teamCode.length() != 4) {
            Snackbar.make(containerView, R.string.enter_valid_code, Snackbar.LENGTH_SHORT).show();

            return;
        }

        API.getInstance().getAPI()
                .registerRescue(teamCode, 0.0, 0.0)
                .enqueue(new Callback<UserResponse>() {
                    @Override
                    public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                        if (response.isSuccessful()) {
                            appData.edit()
                                    .putUserId(response.body().getUser().getUserId())
                                    .putPhone(teamCode)
                                    .putCivilian(false)
                                    .apply();

                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<UserResponse> call, Throwable t) {

                    }
                });
    }

    private void loginCivilian() {
        if (phoneEditText.getText() == null
                || phoneEditText.getText().toString().isEmpty()) {

            Snackbar.make(containerView, R.string.enter_valid_phone, Snackbar.LENGTH_SHORT).show();

            return;
        }

        String phoneNumber = phoneEditText.getText().toString();

        if (!PhoneNumberUtils.isGlobalPhoneNumber(phoneNumber)) {
            Snackbar.make(containerView, R.string.enter_valid_phone, Snackbar.LENGTH_SHORT).show();

            return;
        }

        API.getInstance().getAPI()
                .registerUser("", phoneNumber, 0.0, 0.0)
                .enqueue(new Callback<UserResponse>() {
                    @Override
                    public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                        if (response.isSuccessful()) {
                            appData.edit()
                                    .putUserId(response.body().getUser().getUserId())
                                    .putPhone(phoneNumber)
                                    .putCivilian(true)
                                    .apply();

                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<UserResponse> call, Throwable t) {

                    }
                });
    }
}
