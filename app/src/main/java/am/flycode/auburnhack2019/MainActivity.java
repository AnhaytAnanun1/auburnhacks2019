package am.flycode.auburnhack2019;

import am.flycode.auburnhack2019.model.AppDataPreferences;
import am.flycode.auburnhack2019.model.Constants;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final double E_SAFE_LAT = 32.608317;
    private static final double E_SAFE_LONG = -85.495493;

    @BindView(R.id.map)
    MapView mapView;
    @BindView(R.id.call_center)
    AppCompatButton callCenterButton;
    @BindView(R.id.civilian_prompt)
    View civilianPromptView;

    private AppDataPreferences appData;
    private GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        appData = new AppDataPreferences(this);

        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        if (appData.getCivilian()) {
            callCenterButton.setVisibility(View.GONE);
            civilianPromptView.setVisibility(View.VISIBLE);
        } else {
            callCenterButton.setVisibility(View.VISIBLE);
            civilianPromptView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mapView.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mapView.onResume();

        LocalBroadcastManager
                .getInstance(this)
                .registerReceiver(locationUpdateBroadcastReceiver, new IntentFilter(Constants.ACTION.LOCATION_UPDATE));
    }

    @Override
    protected void onPause() {
        super.onPause();

        mapView.onPause();

        LocalBroadcastManager
                .getInstance(this)
                .unregisterReceiver(locationUpdateBroadcastReceiver);
    }

    @Override
    protected void onStart() {
        super.onStart();

        mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();

        mapView.onStop();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        mapView.onSaveInstanceState(outState);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setAllGesturesEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setIndoorLevelPickerEnabled(true);

        this.googleMap = googleMap;

        if (appData.getCivilian()) {

        }
    }

    @OnClick(R.id.sos)
    public void onSOS(AppCompatButton sosButton) {
        if (!appData.getBeacon()) {
            appData.edit()
                    .putBeacon(true)
                    .apply();

            sosButton.setBackgroundColor(getColor(R.color.safe));
            sosButton.setText(R.string.help_is_on_the_whey);
        }
    }

    private BroadcastReceiver locationUpdateBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (googleMap == null) {
                return;
            }

            if (appData.getTracking()) {
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(appData.getLatitude(), appData.getLongitude()),
                        18
                ));
            }
        }
    };
}
