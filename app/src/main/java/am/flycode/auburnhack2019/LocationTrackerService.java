package am.flycode.auburnhack2019;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.tasks.Task;

import am.flycode.auburnhack2019.model.AppDataPreferences;
import am.flycode.auburnhack2019.model.Constants;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class LocationTrackerService extends Service {

    AppDataPreferences appData;

    @Override
    public IBinder onBind(Intent intent) {
        return  null;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate() {
        super.onCreate();

        appData = new AppDataPreferences(this);

        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(60 * 10 * 1000);

        LocationSettingsRequest locationSettingsRequest = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)
                .build();

        LocationServices
                .getSettingsClient(this)
                .checkLocationSettings(locationSettingsRequest);

        LocationServices
                .getFusedLocationProviderClient(this)
                .requestLocationUpdates(locationRequest, locationCallback, null);
    }

    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            if (locationResult == null) {
                return;
            }

            appData.edit()
                    .putLatitude((float) locationResult.getLastLocation().getLatitude())
                    .putLongitude((float) locationResult.getLastLocation().getLongitude())
                    .apply();

            LocalBroadcastManager
                    .getInstance(LocationTrackerService.this)
                    .sendBroadcast(new Intent(Constants.ACTION.LOCATION_UPDATE));
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();

        LocationServices
                .getFusedLocationProviderClient(this)
                .removeLocationUpdates(locationCallback);
    }
}
