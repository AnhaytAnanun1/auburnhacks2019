package am.flycode.auburnhack2019.model;

import io.t28.shade.annotation.Preferences;
import io.t28.shade.annotation.Property;

@Preferences("am.flycode.auburnhack2019.model")
public interface AppData {
    @Property(key = "phone")
    String phone();

    @Property(key = "userId")
    String userId();

    @Property(key = "latitude", defValue = "0")
    float latitude();

    @Property(key = "longitude", defValue = "0")
    float longitude();

    @Property(key = "tracking", defValue = "true")
    boolean tracking();

    @Property(key = "beacon", defValue = "false")
    boolean beacon();

    @Property(key = "civilian", defValue = "false")
    boolean civilian();
}
