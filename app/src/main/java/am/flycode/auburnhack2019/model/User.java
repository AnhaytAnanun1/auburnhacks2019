package am.flycode.auburnhack2019.model;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("id")
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
