package am.flycode.auburnhack2019.api;

import am.flycode.auburnhack2019.model.UserResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface APIService {
    @FormUrlEncoded
    @POST("/api/register_user")
    Call<UserResponse> registerUser(@Field("name") String name, @Field("phone_number") String phone,
                                    @Field("lat") double latitude, @Field("lng") double longitude);

    @FormUrlEncoded
    @POST("/api/register_help_team")
    Call<UserResponse> registerRescue(@Field("team_code") String phone,
                                    @Field("lat") double latitude, @Field("lng") double longitude);
}
