package am.flycode.auburnhack2019.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class API {
    private static API instance;

    private APIService api;

    public static API getInstance() {
        if (instance == null) {
            instance = new API();
        }

        return instance;
    }

    public API() {
        api = new Retrofit.Builder()
                .baseUrl("https://auhacks.herokuapp.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(APIService.class);
    }

    public APIService getAPI() {
        return api;
    }
}
